package com.mo.payments.domain.ports;

import static org.mockito.ArgumentMatchers.any;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.mo.payments.utils.PaymentFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;


@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

    @Mock private PaymentCommiter paymentCommiter;

    @Mock private PaymentRepository paymentRepository;

    @InjectMocks
    private PaymentService paymentService;

    @Test
    void paymentShouldBeCommittedDuringCreation() throws InvalidPaymentCreatingData, InvalidFilePath, IOException, InvalidPaymentObject {
        // given
        PaymentDTO paymentDTO = PaymentFactory.defaultProperPaymentDTO();
        BDDMockito.given(paymentRepository.save(any())).willReturn(paymentDTO);

        // when
        PaymentRequest paymentRequest = PaymentFactory.defaultProperPaymentRequest();
        paymentService.create(paymentRequest);

        // then
        Mockito.verify(paymentCommiter).commitPayment(any());
    }

    @Test
    void paymentShouldBeCommittedDuringUpdating() throws InvalidPaymentCreatingData, InvalidFilePath, IOException, InvalidPaymentObject {
        // given
        PaymentDTO paymentDTO = PaymentFactory.defaultProperPaymentDTO();
        BDDMockito.given(paymentRepository.update(any(), any())).willReturn(paymentDTO);

        // when
        PaymentRequest paymentRequest = PaymentFactory.defaultProperPaymentRequest();
        paymentService.update("1", paymentRequest);

        // then
        Mockito.verify(paymentCommiter).commitPayment(any());
    }
}