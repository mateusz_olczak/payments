package com.mo.payments.domain.model;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    void amountShouldNotBeEqualsToZero() {
        // given
        Amount amount = new Amount(BigDecimal.ZERO);

        // when
        // then
        assertTrue(amount.isInvalid());
    }

    @Test
    void amountShouldNotBeNegative() {
        // given
        Amount amount = new Amount(BigDecimal.valueOf(-1));

        // when
        // then
        assertTrue(amount.isInvalid());
    }

    @Test
    void amountShouldBePositive() {
        // given
        Amount amount = new Amount(BigDecimal.valueOf(1));

        // when
        // then
        assertFalse(amount.isInvalid());
    }
    @Test
    void amountCanBeFloatingPointNumber() {
        // given
        Amount amount = new Amount(BigDecimal.valueOf(0.01));

        // when
        // then
        assertFalse(amount.isInvalid());
    }

    @Test
    void amountShouldNotBeNegativeFloatingPointNumber() {
        // given
        Amount amount = new Amount(BigDecimal.valueOf(-0.01));

        // when
        // then
        assertTrue(amount.isInvalid());
    }

    @Test
    void amountShouldNotBeNull() {
        // given
        Amount amount = new Amount(null);

        // when
        // then
        assertTrue(amount.isInvalid());
    }

}