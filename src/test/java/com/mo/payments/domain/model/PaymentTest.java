package com.mo.payments.domain.model;

import static org.junit.jupiter.api.Assertions.*;

import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

class PaymentTest {

    private final PaymentId propoerPaymentId = new PaymentId("156");
    private final Amount properAmount = new Amount(BigDecimal.TEN);
    private final Currency currency = Currency.getInstance("PLN");
    private final UserId properUserId = new UserId(1L);
    private final BankAccountNumber properBankAccountNumber = new BankAccountNumber("12345678901234567890");

    private final PaymentId impropoerPaymentId = new PaymentId("");
    private final Amount improperAmount = new Amount(BigDecimal.valueOf(-1));
    private final UserId improperUserId = new UserId(-1L);
    private final BankAccountNumber improperBankAccountNumber = new BankAccountNumber("123456789");

    @Test
    void paymentShouldHaveAmount() {
        // given
        // then
        assertThrows(NullPointerException.class, () -> {
            // when
            Payment payment = new Payment(propoerPaymentId, null, currency, properUserId, properBankAccountNumber);
        });
    }

    @Test
    void paymentShouldHaveProperAmount() {
        // given
        // then
        assertThrows(InvalidPaymentCreatingData.class, () -> {
            // when
            Payment payment = new Payment(propoerPaymentId, improperAmount, currency, properUserId, properBankAccountNumber);
        });
    }

    @Test
    void paymentShouldHaveCurrency() {
        // given
        // then
        assertThrows(InvalidPaymentCreatingData.class, () -> {
            // when
            Payment payment = new Payment(impropoerPaymentId, properAmount, null, properUserId, properBankAccountNumber);
        });
    }

    @Test
    void paymentShouldHaveProperId() {
        // given
        // then
        assertThrows(InvalidPaymentCreatingData.class, () -> {
            // when
            Payment payment = new Payment(impropoerPaymentId, properAmount, currency, properUserId, properBankAccountNumber);
        });
    }
}