package com.mo.payments.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankAccountNumberTest {

    @Test
    void bankAccountIsInvalidWhenIsLessThanMinimalValue() {
        // given
        BankAccountNumber bankAccountNumber = new BankAccountNumber("123456789012345");

        // when
        // then
        assertTrue(bankAccountNumber.isInvalid());
    }

    @Test
    void bankAccountIsInvalidWhenIsGreaterThanMaximalValue() {
        // given
        BankAccountNumber bankAccountNumber = new BankAccountNumber("1234567890123456789012345678901234567");
        // when
        // then
        assertTrue(bankAccountNumber.isInvalid());
    }

    @Test
    void bankAccountIsInvalidWhenContainsCharsDifferentThanNumbers() {
        // given
        BankAccountNumber bankAccountNumber = new BankAccountNumber("123456789012345678990A");
        // when
        // then
        assertTrue(bankAccountNumber.isInvalid());
    }

    @Test
    void bankAccountIsValidWhenIsInProperRange() {
        // given
        BankAccountNumber bankAccountNumber = new BankAccountNumber("123456789012345678990");
        // when
        // then
        assertFalse(bankAccountNumber.isInvalid());
    }

    @Test
    void bankAccountIsInvalidWhenIsNull() {
        // given
        BankAccountNumber bankAccountNumber = new BankAccountNumber(null);
        // when
        // then
        assertTrue(bankAccountNumber.isInvalid());
    }
}