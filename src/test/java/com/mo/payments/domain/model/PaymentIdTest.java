package com.mo.payments.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentIdTest {

    @Test
    void paymentIdShouldNotBe0CharsString() {
        // given
        PaymentId paymentId = new PaymentId("");
        // when
        // then
        assertTrue(paymentId.isInvalid());
    }

    @Test
    void paymentIdShouldContainsSomeChars() {
        // given
        PaymentId paymentId = new PaymentId("123");
        // when
        // then
        assertFalse(paymentId.isInvalid());
    }

    @Test
    void paymentIdShouldNotBeNull() {
        // given
        PaymentId paymentId = new PaymentId(null);
        // when
        // then
        assertTrue(paymentId.isInvalid());
    }
}