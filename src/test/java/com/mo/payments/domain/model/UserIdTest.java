package com.mo.payments.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserIdTest {

    @Test
    void userIdShouldNotBeNegative() {
        // given
        UserId userId = new UserId(-1L);
        // when
        // then
        Assertions.assertTrue(userId.isInvalid());
    }

    @Test
    void userIdShouldNotBeEqualsToZero() {
        // given
        UserId userId = new UserId(0L);
        // when
        // then
        Assertions.assertTrue(userId.isInvalid());
    }

    @Test
    void userIdShouldBePositive() {
        // given
        UserId userId = new UserId(1L);
        // when
        // then
        Assertions.assertFalse(userId.isInvalid());
    }

    @Test
    void userIdShouldNotBeNull() {
        // given
        UserId userId = new UserId(null);
        // when
        // then
        Assertions.assertTrue(userId.isInvalid());
    }
}