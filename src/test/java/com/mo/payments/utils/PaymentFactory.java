package com.mo.payments.utils;

import static java.math.BigDecimal.ONE;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.model.*;

import java.util.Currency;

public class PaymentFactory {

    public static PaymentDTO defaultProperPaymentDTO() throws InvalidPaymentCreatingData {
        return PaymentDTO.of(new PaymentId("1"), defaultProperPaymentRequest());
    }
    public static PaymentRequest defaultProperPaymentRequest() throws InvalidPaymentCreatingData {
        return new PaymentRequest(defaultProperPayment());
    }

    public static Payment defaultProperPayment() throws InvalidPaymentCreatingData {
        return new Payment(new PaymentId("1"), new Amount(ONE), Currency.getInstance("USD"), new UserId(1L), new BankAccountNumber("123456789123456789123456"));
    }
}
