package com.mo.payments.adapters.api.advice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.mo.payments.domain.exception.persistance.csv.LoadDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
@ResponseBody
@Slf4j
class RestErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    String invalidFilePathError(InvalidFilePath ex) {
        return handleException(ex);
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    String invalidPaymentObject(InvalidPaymentObject ex) {
        return handleException(ex);
    }

    @ExceptionHandler
    @ResponseStatus(NOT_ACCEPTABLE)
    String invalidPaymentObjectData(InvalidPaymentCreatingData ex) {
        return handleException(ex);
    }

    @ExceptionHandler
    @ResponseStatus(NOT_ACCEPTABLE)
    Object loadDataException(LoadDataException ex) {
        return handleException(ex);
    }

    @ExceptionHandler
    @ResponseStatus(NOT_ACCEPTABLE)
    Object elementNotFound(NoSuchElementException ex) { return handleException(ex); }

    private String handleException (Throwable ex) {
        String message = ex.getMessage();
        log.error(message, ex);

        return message;
    };

}
