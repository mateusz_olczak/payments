package com.mo.payments.adapters.api;

import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.mo.payments.domain.exception.persistance.csv.LoadDataException;
import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.domain.ports.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("payment")
class PaymentController {

    private final PaymentService paymentService;

    @Autowired
    PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("{paymentId}")
    ResponseEntity<PaymentDTO> getPayment(@PathVariable("paymentId") String paymentId) throws IOException {
        return ResponseEntity
                .ok()
                .body(paymentService.getById(paymentId));
    }

    @GetMapping
    ResponseEntity<List<PaymentDTO>> getAllPayments() throws LoadDataException {
        List<PaymentDTO> allPayments = paymentService.getAll();

         return ResponseEntity
                .ok()
                .body(allPayments);
    }

    @PostMapping
    ResponseEntity<PaymentDTO> createPayment(@RequestBody PaymentRequest payment) throws InvalidPaymentCreatingData, InvalidFilePath, InvalidPaymentObject, IOException {
        PaymentDTO createdPayment = paymentService.create(payment);

        return ResponseEntity
                .created(URI.create("/payment/" + createdPayment.getId()))
                .body(createdPayment);
    }

    @PatchMapping("{paymentId}")
    ResponseEntity<PaymentDTO> updatePayment(@PathVariable("paymentId") String paymentId, @RequestBody PaymentRequest payment) throws InvalidPaymentCreatingData, IOException, InvalidFilePath, InvalidPaymentObject {
        PaymentDTO createdPayment = paymentService.update(paymentId, payment);

        return ResponseEntity
                .ok()
                .body(createdPayment);
    }

}
