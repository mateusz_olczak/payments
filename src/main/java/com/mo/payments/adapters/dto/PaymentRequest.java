package com.mo.payments.adapters.dto;

import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.model.*;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Currency;

@Value
@AllArgsConstructor
public class PaymentRequest {

    BigDecimal amount;
    Currency currency;
    Long userId;
    String bankAccountNumber;

    public PaymentRequest(Payment payment) throws InvalidPaymentCreatingData {
        this.amount = payment.getAmount().getValue();
        this.currency = payment.getCurrency();
        this.userId = payment.getUserId().getId();
        this.bankAccountNumber = payment.getBankAccountNumber().getNumber();

        isInvalid();
    }

    private void isInvalid() throws InvalidPaymentCreatingData {
        if (amount == null || currency == null || userId == null || bankAccountNumber == null) {
            throw new InvalidPaymentCreatingData();
        }
    }

    public Payment toPayment(PaymentId paymentId) throws InvalidPaymentCreatingData {
        return new Payment(
                paymentId,
                new Amount(amount),
                currency,
                new UserId(userId),
                new BankAccountNumber(bankAccountNumber)
        );
    }

}
