package com.mo.payments.adapters.dto;

import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.model.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PaymentDTO {

    String id;
    BigDecimal amount;
    String currency;
    Long userId;
    String bankAccountNumber;

    public static PaymentDTO of(PaymentId id, PaymentRequest payment) {
        return new PaymentDTO(
                id.getId(),
                payment.getAmount(),
                payment.getCurrency().getCurrencyCode(),
                payment.getUserId(),
                payment.getBankAccountNumber());
    }

    public Payment toPayment() throws InvalidPaymentCreatingData {
        return new Payment(
                new PaymentId(id),
                new Amount(amount),
                Currency.getInstance(currency),
                new UserId(userId),
                new BankAccountNumber(bankAccountNumber)
        );
    }
}
