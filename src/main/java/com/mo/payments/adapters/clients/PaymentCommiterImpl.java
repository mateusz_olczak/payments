package com.mo.payments.adapters.clients;

import com.mo.payments.domain.model.Payment;
import com.mo.payments.domain.ports.PaymentCommiter;
import org.springframework.stereotype.Component;

@Component
class PaymentCommiterImpl implements PaymentCommiter {

    @Override
    public void commitPayment(Payment payment) {
        //TODO
        // call to external micro service / API
    }

}
