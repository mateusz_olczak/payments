package com.mo.payments.adapters.persistance.csv;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CsvProcessorsConfig {

    @Value("${csv.location}")
    private String path;

    @Value("${csv.name}")
    private String fileName;

    @Bean
    public CsvReader csvReader() {
        return new CsvReader(path, fileName);
    }

    @Bean
    public CsvPersister csvPersister() {
        return new CsvPersister(path, fileName);
    }

}
