package com.mo.payments.adapters.persistance.csv;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.mo.payments.domain.model.PaymentId;
import com.mo.payments.domain.ports.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Repository
class PaymentRepositoryImpl implements PaymentRepository {

    private final CsvPersister csvPersister;
    private final CsvReader csvReader;

    private final PaymentIdGenerator paymentIdGenerator;

    @Autowired
    PaymentRepositoryImpl(CsvPersister csvPersister, CsvReader csvReader, PaymentIdGenerator paymentIdGenerator) {
        this.csvPersister = csvPersister;
        this.csvReader = csvReader;
        this.paymentIdGenerator = paymentIdGenerator;
    }

    @Override
    public PaymentDTO save(PaymentRequest payment) throws InvalidFilePath, InvalidPaymentObject, InvalidPaymentCreatingData, IOException {
        PaymentId paymentId = paymentIdGenerator.generateId();
        PaymentDTO paymentDTO = PaymentDTO.of(paymentId, payment);

        synchronized (this) {
            List<PaymentDTO> paymentDTOs = csvReader.loadAll();
            addCurrentPayment(paymentDTO, paymentDTOs, paymentId);
        }

        return paymentDTO;
    }

    @Override
    public List<PaymentDTO> loadAll() throws IOException {
        return csvReader.loadAll();
    }

    @Override
    public PaymentDTO loadById(String paymentId) throws IOException {
        return csvReader.load(paymentId);
    }

    @Override
    public PaymentDTO update(String id, PaymentRequest paymentRequest) throws IOException, InvalidPaymentCreatingData, InvalidFilePath, InvalidPaymentObject {
        PaymentId paymentId = new PaymentId(id);
        PaymentDTO paymentDTO = PaymentDTO.of(paymentId, paymentRequest);

        synchronized (this) {
            List<PaymentDTO> paymentDTOs = csvReader.loadAll();
            paymentDTOs.removeIf(payment -> payment.getId().equals(id));

            addCurrentPayment(paymentDTO, paymentDTOs, paymentId);
        }

        return paymentDTO;
    }

    private void addCurrentPayment(PaymentDTO paymentDTO, List<PaymentDTO> paymentDTOs, PaymentId paymentId) throws InvalidFilePath, InvalidPaymentObject {
        paymentDTOs.add(paymentDTO);
        csvPersister.persistAll(paymentDTOs);
    }

}
