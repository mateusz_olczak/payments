package com.mo.payments.adapters.persistance.csv;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.NoSuchElementException;

import static java.nio.charset.StandardCharsets.UTF_8;

@AllArgsConstructor
class CsvReader {

    private String path;
    private String fileName;

    List<PaymentDTO> loadAll() throws IOException {
        try (Reader reader = prepareFileBuffer()) {

            CsvToBean<PaymentDTO> csvToBean = createCsvToBean(reader);

            return csvToBean.parse();
        }
    }

    PaymentDTO load(String id) throws IOException {
        try (Reader reader = prepareFileBuffer()) {

            CsvToBean<PaymentDTO> csvToBean = createCsvToBean(reader);

            final String errorMessage = String.format("No element with ID %s has been found.", id);
            return csvToBean
                    .stream()
                    .filter(it -> id.equals(it.getId()))
                    .findFirst()
                    .orElseThrow(() -> {
                        return new NoSuchElementException(errorMessage);
                    });
        }
    }

    private BufferedReader prepareFileBuffer() throws IOException {
        return Files.newBufferedReader(Path.of(path + fileName), UTF_8);
    }

    private CsvToBean<PaymentDTO> createCsvToBean(Reader reader) {
        return new CsvToBeanBuilder<PaymentDTO>(reader)
                .withType(PaymentDTO.class)
                .withSeparator(',')
                .withIgnoreLeadingWhiteSpace(true)
                .build();
    }

}
