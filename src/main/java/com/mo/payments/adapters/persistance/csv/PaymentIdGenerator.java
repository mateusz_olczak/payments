package com.mo.payments.adapters.persistance.csv;

import com.mo.payments.domain.model.PaymentId;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
class PaymentIdGenerator {

    PaymentId generateId() {
        return new PaymentId(UUID.randomUUID().toString());
    }
}
