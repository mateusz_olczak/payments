package com.mo.payments.adapters.persistance.csv;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


@AllArgsConstructor
class CsvPersister {

    private String path;
    private String fileName;

    PaymentDTO persist(PaymentDTO payment) throws InvalidFilePath, InvalidPaymentObject {
        Path path = getPath();

        try (Writer writer = Files.newBufferedWriter(path, UTF_8, APPEND)) {
            StatefulBeanToCsv<PaymentDTO> beanToCsv = prepareStatefulBeanToCsv(writer);
            beanToCsv.write(payment);

            return payment;
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            throw new InvalidPaymentObject(e);
        }
    }

    private Path getPath() throws InvalidFilePath {
        Path path = Path.of(this.path + fileName);
        if (path == null) {
            throw new InvalidFilePath();
        }
        return path;
    }

    public void persistAll(List<PaymentDTO> paymentDTOs) throws InvalidFilePath, InvalidPaymentObject {
        Path path = getPath();

        try (Writer writer = Files.newBufferedWriter(path, UTF_8, WRITE)) {
            StatefulBeanToCsv<PaymentDTO> beanToCsv = prepareStatefulBeanToCsv(writer);
            beanToCsv.write(paymentDTOs);
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            throw new InvalidPaymentObject(e);
        }
    }

    private StatefulBeanToCsv<PaymentDTO> prepareStatefulBeanToCsv(Writer writer) {
        return new StatefulBeanToCsvBuilder<PaymentDTO>(writer)
                .withSeparator(',')
                .withOrderedResults(true)
                .withQuotechar(CSVWriter.DEFAULT_ESCAPE_CHARACTER)
                .build();
    }

}
