package com.mo.payments.domain.exception.parse;

public class InvalidPaymentCreatingData extends Exception {

    public InvalidPaymentCreatingData() {
        super("Unable to create Payment object due to invalid input data!");
    }
}
