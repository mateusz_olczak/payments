package com.mo.payments.domain.exception.persistance.csv;

public class LoadDataException extends Throwable{

    public LoadDataException(Exception e) {
        super("Unable to load data from file.", e);
    }
}
