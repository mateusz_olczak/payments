package com.mo.payments.domain.exception.persistance.csv;

public class InvalidFilePath extends Exception {

    public InvalidFilePath() {
        super("Directory data has not been created in project resources.");
    }

}
