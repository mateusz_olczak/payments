package com.mo.payments.domain.exception.persistance.csv;

public class InvalidPaymentObject extends Throwable {

    public InvalidPaymentObject(Exception e) {
        super("Please fix the input data.", e);
    }

}
