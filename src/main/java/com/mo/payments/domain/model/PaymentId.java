package com.mo.payments.domain.model;

import lombok.Value;

@Value
public class PaymentId {

    String id;

    public boolean isInvalid() {
        return id == null || id.length() <= 0;
    }

}
