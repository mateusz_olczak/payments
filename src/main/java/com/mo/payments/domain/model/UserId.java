package com.mo.payments.domain.model;

import lombok.Value;

@Value
public class UserId {

    Long id;

    boolean isInvalid() {
        return id == null || id <= 0;
    }
}
