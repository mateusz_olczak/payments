package com.mo.payments.domain.model;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;

@Value
@AllArgsConstructor
public class Amount {

    BigDecimal value;

    public boolean isInvalid() {
        return value == null || value.compareTo(BigDecimal.ZERO) <= 0;
    }
}
