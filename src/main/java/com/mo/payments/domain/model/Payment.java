package com.mo.payments.domain.model;

import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import lombok.Value;

import java.util.Currency;

@Value
public class Payment {

    PaymentId id;
    Amount amount;
    Currency currency;
    UserId userId;
    BankAccountNumber bankAccountNumber;

    public Payment(PaymentId id, Amount amount, Currency currency, UserId userId, BankAccountNumber bankAccountNumber) throws InvalidPaymentCreatingData {

        validateCreating(id, amount, currency, userId, bankAccountNumber);

        this.id = id;
        this.amount = amount;
        this.currency = currency;
        this.userId = userId;
        this.bankAccountNumber = bankAccountNumber;
    }

    private void validateCreating(PaymentId paymentId, Amount amount, Currency currency, UserId userId, BankAccountNumber bankAccountNumber) throws InvalidPaymentCreatingData {
        if (paymentId.isInvalid() ||
                amount.isInvalid() ||
                userId.isInvalid() ||
                bankAccountNumber.isInvalid()) {
            throw new InvalidPaymentCreatingData();
        }
    }

}