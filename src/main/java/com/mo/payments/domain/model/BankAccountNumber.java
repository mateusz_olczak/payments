package com.mo.payments.domain.model;

import lombok.Value;
import org.apache.commons.lang3.math.NumberUtils;

@Value
public class BankAccountNumber {

    private static final int MIN_ACCOUNT_NUMBER_LENGTH = 16;
    private static final int MAX_ACCOUNT_NUMBER_LENGTH = 31;

    String number;

    public boolean isInvalid() {
        if (number == null) return true;

        int length = number.length();
        return length < MIN_ACCOUNT_NUMBER_LENGTH
                || length > MAX_ACCOUNT_NUMBER_LENGTH
                || !NumberUtils.isDigits(number);
    }

}
