package com.mo.payments.domain.ports;

import com.mo.payments.domain.model.Payment;

public interface PaymentCommiter {
    void commitPayment(Payment payment);
}
