package com.mo.payments.domain.ports;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;

import java.io.IOException;
import java.util.List;

public interface PaymentRepository {

    PaymentDTO save(PaymentRequest payment) throws InvalidFilePath, InvalidPaymentObject, InvalidPaymentCreatingData, IOException;

    List<PaymentDTO> loadAll() throws IOException;

    PaymentDTO loadById(String paymentId) throws IOException;

    PaymentDTO update(String id, PaymentRequest payment) throws IOException, InvalidPaymentCreatingData, InvalidFilePath, InvalidPaymentObject;

}
