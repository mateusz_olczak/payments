package com.mo.payments.domain.ports;

import com.mo.payments.adapters.dto.PaymentDTO;
import com.mo.payments.adapters.dto.PaymentRequest;
import com.mo.payments.domain.exception.parse.InvalidPaymentCreatingData;
import com.mo.payments.domain.exception.persistance.csv.InvalidFilePath;
import com.mo.payments.domain.exception.persistance.csv.InvalidPaymentObject;
import com.mo.payments.domain.exception.persistance.csv.LoadDataException;
import com.mo.payments.domain.model.Payment;
import com.mo.payments.domain.model.PaymentId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class PaymentService {

    private final PaymentRepository paymentRepositiry;

    private final PaymentCommiter paymentCommiter;

    @Autowired
    public PaymentService(PaymentRepository paymentRepositiry, PaymentCommiter paymentCommiter) {
        this.paymentRepositiry = paymentRepositiry;
        this.paymentCommiter = paymentCommiter;
    }

    public PaymentDTO create(PaymentRequest paymentRequest) throws InvalidPaymentCreatingData, InvalidFilePath, InvalidPaymentObject, IOException {
        PaymentDTO paymentDTO = paymentRepositiry.save(paymentRequest);

        Payment payment = paymentDTO.toPayment();
        paymentCommiter.commitPayment(payment);

        return paymentDTO;
    }

    public PaymentDTO getById(String paymentId) throws IOException {
        PaymentId paymentIdToValidate = new PaymentId(paymentId);
        if (paymentIdToValidate.isInvalid()) {
            throw new IllegalArgumentException();
        }

        return paymentRepositiry.loadById(paymentId);
    }

    public List<PaymentDTO> getAll() throws LoadDataException {
        try {
            return paymentRepositiry.loadAll();
        } catch (IOException e) {
            throw new LoadDataException(e);
        }
    }

    public PaymentDTO update(String id, PaymentRequest paymentRequest) throws InvalidPaymentCreatingData, IOException, InvalidFilePath, InvalidPaymentObject {
        Payment payment = paymentRequest.toPayment(new PaymentId(id));
        paymentCommiter.commitPayment(payment);

        return paymentRepositiry.update(id, paymentRequest);
    }

}
